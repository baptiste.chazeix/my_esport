# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = My_esport

CONFIG += sailfishapp

SOURCES += src/My_esport.cpp \
    src/game.cpp \
    src/gamelist.cpp \
    src/gamemodel.cpp \
    src/manager.cpp \
    src/team.cpp \
    src/teamlist.cpp \
    src/teammodel.cpp

DISTFILES += qml/My_esport.qml \
    ../../../Téléchargements/games.json \
    qml/cover/CoverPage.qml \
    qml/pages/GamesPage.qml \
    qml/pages/TeamsPage.qml \
    rpm/My_esport.changes.in \
    rpm/My_esport.changes.run.in \
    rpm/My_esport.spec \
    rpm/My_esport.yaml \
    translations/*.ts \
    My_esport.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/My_esport-de.ts

HEADERS += \
    src/game.h \
    src/gamelist.h \
    src/gamemodel.h \
    src/manager.h \
    src/team.h \
    src/teamlist.h \
    src/teammodel.h

RESOURCES += \
    data.qrc
