# My Esport

## Description du sujet

Ce projet permet de voir les principaux jeux esport.
Sur chacun de ces jeux, on peut voir leurs teams compétitives.

Il se décompose en 2 pages:

- Page d'accueil (Master): liste des jeux compétitifs, choix du jeu.
- Page Détails: liste des équipes compétitives sur le jeu choisi.

## Points techniques abordés


### Fichier JSON

- Lecture d'un fichier JSON pour la liste des jeux du master.
- Données parsées pour afficher les noms des jeux.

### Appel de l'API PandaScore 

- Appel API à partir de l'id du jeu sélectionné. 
- Données parsées pour afficher le nom des équipes. Nous possédons d'autres valeurs comme la localisation, l'image, ... mais dans cette première version de l'application, nous n'affichons seulement que les noms des équipes.


## Fonctionnalités à ne pas râter lors des tests

## Bugs connus



## Auteurs

### Baptiste Chazeix

- 50% du travail
- QML (vues): toutes les pages + tout le fonctionnement de l'application
- Auto-Évaluation: je me mettrais 14 pour ce projet.

### Maxime Bavouzet

- 50% du travail
- C++ (models): tout sauf le manager + tout le fonctionnement de l'application
- Auto-Évaluation: je me mettrais 14 pour ce projet.
