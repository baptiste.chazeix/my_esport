#include "gamelist.h"

GameList::GameList(QObject *parent): QObject(parent)
{

}

void GameList::append(Game *game)
{
    m_games.append(game);
}

QList<Game *> GameList::games() const
{
    return m_games;
}

Game *GameList::at(int pos) const
{
    return m_games.at(pos);
}

int GameList::count() const
{
    return m_games.size();
}
