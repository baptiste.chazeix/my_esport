#include "gamemodel.h"
#include "gamelist.h"
#include "game.h"

GameModel::GameModel(GameList *gameList, QObject *parent): QAbstractListModel(parent), m_gameList(nullptr)
{
    if(gameList)
        setList(gameList);
}

GameList *GameModel::list() const
{
    return m_gameList;
}

int GameModel::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid())
        return 0;
    return m_gameList->count();
}

QVariant GameModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    auto game = m_gameList->at(index.row());
    switch(role) {
        case Name :
            return game->name();
        case Id :
            return game->id_game();
    }

    return QVariant();
}

void GameModel::setList(GameList *gameList)
{
    beginResetModel();
    m_gameList = gameList;
    endResetModel();
}
