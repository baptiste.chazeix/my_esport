#ifndef TEAM_H
#define TEAM_H

#include <QObject>

class Team : public QObject
{
    Q_OBJECT

public:
    Team(const int &id, const QString &name, const QString &image_url , const QString &location);

    int id() const;
    QString name() const;
    QString image_url() const;
    QString location() const;

private:
    int m_id;
    QString m_name;
    QString m_image_url;
    QString m_location;
};

#endif // TEAM_H
