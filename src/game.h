#ifndef GAME_H
#define GAME_H

#include <QObject>

class Game : public QObject
{
    Q_OBJECT

public:
    Game(const int &id_game, const QString &name);

    int id_game() const;
    QString name() const;

private:
    int m_id_game;
    QString m_name;
};

#endif // GAME_H
