#include "teamlist.h"

TeamList::TeamList(QObject *parent) : QObject(parent)
{

}

void TeamList::append(Team *team)
{
    m_teams.append(team);
    emit TeamList::listUpdated();
}

QList<Team *> TeamList::teams() const
{
    return m_teams;
}

Team *TeamList::at(int pos) const
{
    return m_teams.at(pos);
}

int TeamList::count() const
{
    return m_teams.size();
}

void TeamList::clear()
{
    m_teams.clear();
}
