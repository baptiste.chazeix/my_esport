#include "teammodel.h"
#include "teamlist.h"
#include "team.h"


TeamModel::TeamModel(TeamList *teamList, QObject *parent): QAbstractListModel(parent), m_teamList(nullptr)
{
   if(teamList){
       setList(teamList);
   }
}

TeamList *TeamModel::list() const
{
    return m_teamList;
}

int TeamModel::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid())
        return 0;
    return m_teamList->count();
}

QVariant TeamModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    auto team = m_teamList->at(index.row());
    switch(role) {
        case Name :
            return team->name();
    }

    return QVariant();
}

void TeamModel::setList(TeamList *teamList)
{
    beginResetModel();
    m_teamList = teamList;
    endResetModel();
}
