#include "game.h"

Game::Game(const int &id_game, const QString &name): m_id_game(id_game), m_name(name)
{

}

int Game::id_game() const
{
    return m_id_game;
}

QString Game::name() const
{
    return m_name;
}
