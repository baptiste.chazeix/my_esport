#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include "manager.h"


int main(int argc, char *argv[])
{
    QGuiApplication * app = SailfishApp::application(argc, argv);
    QQuickView * view     = SailfishApp::createView();

    Manager manager;
    manager.loadGames();

    view->rootContext()->setContextProperty("gameModel", manager.gameModel());
    view->rootContext()->setContextProperty("teamModel", manager.teamModel());

    view->rootContext()->setContextProperty("manager", &manager);

    view->setSource(SailfishApp::pathToMainQml());
    view->show();

    return app->exec();
}
