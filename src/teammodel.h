#ifndef TEAMMODEL_H
#define TEAMMODEL_H

#include <QAbstractListModel>

class TeamList;

class TeamModel : public QAbstractListModel
{
    Q_OBJECT

    TeamList* m_teamList;

public:
    TeamModel(TeamList* teamList, QObject* parent = nullptr);
    TeamList* list() const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setList(TeamList* teamList);

    enum {
        Name
    };

    QHash<int, QByteArray> roleNames() const override
    {
        QHash<int, QByteArray> roles;

        roles[Name] = "name";

        return roles;
    }
};

#endif // TEAMMODEL_H
