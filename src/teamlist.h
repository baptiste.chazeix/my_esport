#ifndef TEAMLIST_H
#define TEAMLIST_H

#include <QObject>

class Team;

class TeamList : public QObject
{
    Q_OBJECT

    QList<Team*> m_teams;

public:
    explicit TeamList(QObject *parent = nullptr);
    void append(Team* team);
    QList<Team*> teams() const;
    Team* at(int pos) const;
    int count() const;
    void clear();

signals:
    void listUpdated();
};

#endif // TEAMLIST_H
