#ifndef GAMELIST_H
#define GAMELIST_H

#include <QObject>

class Game;

class GameList: public QObject
{
    Q_OBJECT

    QList<Game*> m_games;

public:
    explicit GameList(QObject *parent = nullptr);
    void append(Game* game);
    QList<Game*> games() const;
    Game* at(int pos) const;
    int count() const;
};

#endif // GAMELIST_H
