#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include <QAbstractListModel>

class GameList;

class GameModel : public QAbstractListModel
{
    Q_OBJECT

    GameList* m_gameList;

public:
    GameModel(GameList* gameList, QObject* parent = nullptr);
    GameList* list() const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setList(GameList* gameList);

    enum {
        Name,
        Id
    };

    QHash<int, QByteArray> roleNames() const override
    {
        QHash<int, QByteArray> roles;

        roles[Name] = "name";
        roles[Id] = "id";

        return roles;
    }
};

#endif // GAMEMODEL_H
