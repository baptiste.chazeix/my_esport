#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>

#include "game.h"
#include "gamemodel.h"
#include "gamelist.h"
#include "teamlist.h"
#include "teammodel.h"
#include "team.h"

class Manager : public QObject
{
    Q_OBJECT

    GameModel m_gameModel;
    GameList m_gameList;

    TeamModel m_teamModel;
    TeamList m_teamList;
    QNetworkAccessManager m_networkManager;

public:
    explicit Manager(QObject *parent = nullptr);
    GameModel* gameModel();
    TeamModel* teamModel();
    void loadGames();

    Q_INVOKABLE void startLoadingTeams(int id);

private:
    void parseGame(const QJsonObject &game);
    void loadTeams(QNetworkReply* reply);
    void parseTeam(const QJsonObject &team);

public slots:
    void updateTeamList();
};

#endif // MANAGER_H
