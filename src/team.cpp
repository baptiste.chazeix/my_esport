#include "team.h"

Team::Team(const int &id, const QString &name, const QString &image_url, const QString &location):
    m_id(id), m_name(name), m_image_url(image_url), m_location(location)
{

}

int Team::id() const
{
    return m_id;
}

QString Team::name() const
{
    return m_name;
}

QString Team::image_url() const
{
    return m_image_url;
}

QString Team::location() const
{
    return m_location;
}
