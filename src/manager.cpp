#include "manager.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QUrl>
#include <QNetworkRequest>

#include <QDebug>

Manager::Manager(QObject *parent): QObject(parent),
    m_gameModel(&m_gameList, this),
    m_teamModel(&m_teamList, this)
{
    connect(&m_networkManager, &QNetworkAccessManager::finished, this, &Manager::loadTeams);
    connect(&m_teamList, &TeamList::listUpdated, this, &Manager::updateTeamList);
}

GameModel *Manager::gameModel()
{
    return &m_gameModel;
}

TeamModel *Manager::teamModel()
{
    return &m_teamModel;
}

void Manager::loadGames()
{
    QByteArray data;
    {
        QFile file(":/res/games.json");

        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            throw "Can't open games.json";

        data = file.readAll();
    }

    QJsonDocument jdoc { QJsonDocument::fromJson(data) };

    QJsonObject json { jdoc.object() };

    if ( json.contains("games") && json["games"].isArray() ) {
        QJsonArray games = json["games"].toArray();

        for ( int i = 0 ; i < games.size() ; ++i ) {
            parseGame(games[i].toObject());
        }
        m_gameModel.setList(&m_gameList);
    }
}

void Manager::startLoadingTeams(int id)
{
    m_teamList.clear();
    QString path = "https://api.pandascore.co/teams?filter[videogame_id]="
            +QString::number(id)
            +"&token=Aw5CFfxlKuWT5LphNBbYvCNakNVVgMvudxkDa7-JjtFvWImna50";
    QUrl url(path);

    QNetworkRequest request;
    request.setUrl(url);    
    m_networkManager.get(request);
}

void Manager::parseGame(const QJsonObject& game) {
    Game *m_game = new Game(game["id"].toInt(), game["name"].toString());
    m_gameList.append(m_game);
}

void Manager::loadTeams(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Network Error" << reply->error();
        throw "Network Error";
    }

    QByteArray data = reply->readAll();
    QJsonDocument jdoc { QJsonDocument::fromJson(data) };
    QJsonArray json { jdoc.array() };
    QJsonArray teams = json.at(0).toArray();

    if (!teams.isEmpty()) {
        for ( int i = 0 ; i < teams.size() ; ++i ) {
            parseTeam(teams[i].toObject());
        }
    }
}

void Manager::parseTeam(const QJsonObject &teamJ)
{
    Team *team;

    if ( teamJ["id"].isDouble() &&
         teamJ["name"].isString() &&
         (teamJ["image_url"].isString() || teamJ["image_url"].isNull()) &&
         (teamJ["location"].isString() || teamJ["location"].isNull()))
        team = new Team(teamJ["id"].toInt(), teamJ["name"].toString(), teamJ["image_url"].toString(), teamJ["location"].toString());
    else {
        team = new Team(-1,"ERROR_NAME","ERROR_IMAGE","ERROR_LOCATION");
    }
    m_teamList.append(team);
}

void Manager::updateTeamList()
{
    m_teamModel.setList(&m_teamList);
}
