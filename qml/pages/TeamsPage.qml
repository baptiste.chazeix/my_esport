import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    property string gameName
    id: teamsPage
    allowedOrientations: Orientation.All


    SilicaListView {
        id: teamList
        model: teamModel
        anchors.fill: parent
        header: PageHeader {
            title: "Teams of " + gameName
        }
        delegate: ListItem {
            id: delegate
            Label {
                x: Theme.horizontalPageMargin
                text: name
                anchors.verticalCenter: parent.verticalCenter
                color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
        }
        VerticalScrollDecorator {}
    }

}
