import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: gamesPage
    allowedOrientations: Orientation.All

    SilicaListView {
        id: gameList
        model: gameModel
        anchors.fill: parent
        header: PageHeader {
            title: "All Games"
        }
        delegate: ListItem {
            id: delegate
            Label {
                x: Theme.horizontalPageMargin
                text: name
                anchors.verticalCenter: parent.verticalCenter
                color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            onClicked: {
                manager.startLoadingTeams(id)
                pageStack.push(Qt.resolvedUrl("TeamsPage.qml"),{gameName: name})
            }
        }
        VerticalScrollDecorator {}
    }

}
